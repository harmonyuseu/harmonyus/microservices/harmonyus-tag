package eu.harmonyus.tag.resource.mapper;

import eu.harmonyus.tag.model.TagEntity;
import eu.harmonyus.tag.resource.model.Tag;

import java.util.ArrayList;
import java.util.List;

public final class TagMapper {

    private TagMapper() {

    }

    public static Tag fromEntity(TagEntity tagEntity) {
        Tag tag = new Tag();
        tag.code = tagEntity.getCode();
        tag.name = tagEntity.getName();
        return tag;
    }

    public static List<Tag> fromEntityList(List<TagEntity> tagEntities) {
        List<Tag> tags = new ArrayList<>();
        tagEntities.forEach(tagEntity -> tags.add(fromEntity(tagEntity)));
        return tags;
    }

    public static void toEntity(TagEntity tagEntity, Tag tag) {
        if (tag.code != null) {
            tagEntity.setCode(tag.code);
        }
        tagEntity.setName(tag.name);
    }
}
