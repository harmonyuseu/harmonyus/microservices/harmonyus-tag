package eu.harmonyus.tag.resource;

import eu.harmonyus.tag.model.TagEntity;
import eu.harmonyus.tag.resource.model.Tag;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import io.vertx.core.http.HttpHeaders;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

@QuarkusTest
class TagResourceTest {

    @ConfigProperty(name = "quarkus.resteasy.path")
    String restPath;

    @BeforeEach
    void setup() {
        TagEntity.deleteAll().await().indefinitely();
    }

    @Test
    @DisplayName("Test - When Calling POST - /api/{version}/tags should create resource - 201")
    void testCreateTag() {
        Tag tag = createTag();

        given()
                .when()
                .body(tag)
                .contentType(ContentType.JSON)
                .post(restPath + "/tags")
                .then()
                .statusCode(201)
                .header(HttpHeaders.LOCATION.toString(), containsString(restPath + "/tags/test-tag"))
                .body("name", is("test tag"))
                .body("code", is("test-tag"));
    }

    @Test
    @DisplayName("Test - When Calling DELETE - /api/{version}/tags/{id} should delete resource - 204")
    void testDeleteTag() {
        createTagEntity("test tag");

        given()
                .when()
                .get(restPath + "/tags/test-tag")
                .then()
                .statusCode(200);

        given()
                .when()
                .delete(restPath + "/tags/test-tag")
                .then()
                .statusCode(204);

        given()
                .when()
                .get(restPath + "/tags/test-tag")
                .then()
                .statusCode(404);
    }

    @Test
    @DisplayName("Test - When Calling PUT - /api/{version}/tags/{id} should update resource - 200")
    void testUpdateTag() {
        createTagEntity("ag");

        given()
                .when()
                .get(restPath + "/tags/ag")
                .then()
                .statusCode(200);

        Tag tag = createTag();

        given()
                .when()
                .body(tag)
                .contentType(ContentType.JSON)
                .put(restPath + "/tags/ag")
                .then()
                .statusCode(200);

        given()
                .when()
                .get(restPath + "/tags/test-tag")
                .then()
                .statusCode(200)
                .body("name", is("test tag"))
                .body("code", is("test-tag"));
    }

    @Test
    @DisplayName("Test - When Calling GET - /api/{version}/tags should return resources - 200")
    void testGetAll() {
        createTagEntity("tag1");
        createTagEntity("tag2");
        createTagEntity("tag3");
        createTagEntity("tag4");
        Tag[] tags = given()
                .when()
                .get(restPath + "/tags")
                .then()
                .statusCode(200)
                .extract()
                .as(Tag[].class);
        assertThat("Tags length", tags.length, equalTo(4));
        assertTagValue(tags[0], "tag1", "tag1");
        assertTagValue(tags[1], "tag2", "tag2");
        assertTagValue(tags[2], "tag3", "tag3");
        assertTagValue(tags[3], "tag4", "tag4");
    }

    private void assertTagValue(Tag tag, String name, String code) {
        assertThat(tag.name, equalTo(name));
        assertThat(tag.code, equalTo(code));
    }

    private void createTagEntity(String name) {
        TagEntity tagEntity = new TagEntity();
        tagEntity.setName(name);
        tagEntity.persist().await().indefinitely();
    }

    private Tag createTag() {
        Tag tag = new Tag();
        tag.name = "test tag";
        tag.code = "test-tag";
        return tag;
    }
}
