package eu.harmonyus.tag.resource;

import eu.harmonyus.tag.model.TagEntity;
import eu.harmonyus.tag.resource.mapper.TagMapper;
import eu.harmonyus.tag.resource.model.Tag;
import io.quarkus.mongodb.panache.reactive.ReactivePanacheMongoEntityBase;
import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.time.Duration;
import java.util.List;

@Path("/tags")
@RequestScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TagResource {

    @ConfigProperty(name = "harmonyus.tag-resource.get-all.timeout", defaultValue = "2s")
    Duration getAllTimeout;
    @ConfigProperty(name = "harmonyus.tag-resource.get-by-code.timeout", defaultValue = "2s")
    Duration getByCodeTimeout;
    @ConfigProperty(name = "harmonyus.tag-resource.create.timeout", defaultValue = "2s")
    Duration createTimeout;
    @ConfigProperty(name = "harmonyus.tag-resource.modify.timeout", defaultValue = "2s")
    Duration modifyTimeout;
    @ConfigProperty(name = "harmonyus.tag-resource.delete.timeout", defaultValue = "2s")
    Duration deleteTimeout;
    @ConfigProperty(name = "harmonyus.tag-resource.get-all.max-retries", defaultValue = "1")
    long getAllMaxRetries;
    @ConfigProperty(name = "harmonyus.tag-resource.get-by-code.max-retries", defaultValue = "1")
    long getByCodeMaxRetries;
    @ConfigProperty(name = "harmonyus.tag-resource.create.max-retries", defaultValue = "1")
    long createMaxRetries;
    @ConfigProperty(name = "harmonyus.tag-resource.modify.max-retries", defaultValue = "1")
    long modifyMaxRetries;
    @ConfigProperty(name = "harmonyus.tag-resource.delete.max-retries", defaultValue = "1")
    long deleteMaxRetries;

    @GET
    public Uni<List<Tag>> getAll() {
        Uni<List<TagEntity>> tagsEntitiesUni = TagEntity.listAll();
        return tagsEntitiesUni
                .ifNoItem().after(getAllTimeout).fail()
                .onItem().transform(TagMapper::fromEntityList)
                .onFailure().retry().atMost(getAllMaxRetries);
    }

    @GET
    @Path("/{code}")
    public Uni<Tag> getByCode(@PathParam("code") String code) {
        Uni<TagEntity> tagEntityUniUni = TagEntity.findById(code);
        return tagEntityUniUni
                .ifNoItem().after(getByCodeTimeout).fail()
                .onItem().ifNull().failWith(NotFoundException::new)
                .map(TagMapper::fromEntity)
                .onFailure().retry().atMost(getByCodeMaxRetries);
    }

    @POST
    public Uni<Response> create(Tag tag) {
        TagEntity tagEntity = new TagEntity();
        TagMapper.toEntity(tagEntity, tag);
        return tagEntity.persist()
                .ifNoItem().after(createTimeout).fail()
                .map(unused -> Response.created(URI.create(String.format("/tags/%s", tagEntity.getCode())))
                        .entity(TagMapper.fromEntity(tagEntity)).build())
                .onFailure().retry().atMost(createMaxRetries);
    }

    @PUT
    @Path("/{code}")
    public Uni<Tag> modify(@PathParam("code") String code, Tag tag) {
        Uni<TagEntity> tagEntityUni = TagEntity.findById(code);
        return tagEntityUni
                .ifNoItem().after(modifyTimeout).fail()
                .onItem().ifNull().failWith(NotFoundException::new)
                .flatMap(tagEntity -> {
                    TagMapper.toEntity(tagEntity, tag);
                    return tagEntity.persistOrUpdate().onItem().transform(unused -> tagEntity);
                })
                .onItem().transform(TagMapper::fromEntity)
                .onFailure().retry().atMost(modifyMaxRetries);
    }

    @DELETE
    @Path("/{code}")
    public Uni<Response> delete(@PathParam("code") String code) {
        Uni<TagEntity> tagEntityUni = TagEntity.findById(code);
        return tagEntityUni
                .ifNoItem().after(deleteTimeout).fail()
                .onItem().ifNull().failWith(NotFoundException::new)
                .flatMap(ReactivePanacheMongoEntityBase::delete)
                .map(unused -> Response.noContent().build())
                .onFailure().retry().atMost(deleteMaxRetries);
    }
}
