package eu.harmonyus.tag.resource.model;

import io.quarkus.runtime.annotations.RegisterForReflection;

@RegisterForReflection
public class Tag {
    public String name;
    public String code;
}
